#### HEAD ##############
# Class   : NouvelleEquipe
# Content : Formulaire de création d'une nouvelle équipe.


#### IMPORT ############
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Length
from wtforms import StringField, validators


#### CLASS #############
class NouvelleEquipe(FlaskForm):
    avatar = FileField(
        "Avatar",
        validators = [
            FileAllowed(['jpg', 'png'], 'Seulement des images !')
        ]
    )
    nom = StringField(
        "Nom",
        validators = [
            DataRequired("S'il vous plaît, entrer un nom d'équipe."),
            Length(min=3, max=50, message="Le nom de l'équipe doit comporter 3 caratères minimum et 50 maximum.")
        ]
    )
    poste = StringField(
        "Poste", 
        validators = [
            Length(min=3, max=15, message="Le nom de l'équipe doit comporter 3 caratères minimum et 15 maximum.")
        ]
    )

    def __repr__(self):
        return "<NouvelleEquipe (%s) %s>" % (self.nom.data, self.poste.data)