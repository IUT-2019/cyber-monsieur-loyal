#### HEAD ##############
# Class   : NouveauJoueur
# Content : Formulaire de création d'un nouveau joueur.


#### IMPORT ############
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Length
from wtforms import StringField, SelectField, validators


#### CLASS #############
class NouveauJoueur(FlaskForm):
    nom = StringField(
        "Nom",
        validators = [
            DataRequired("S'il vous plaît, entrer le nom du joueur."),
            Length(min=3, max=50, message="Le nom du joueur doit comporter 3 caratères minimum et 50 maximum.")
        ]
    )
    equipe = SelectField(
        "Equipes",
        choices = list()
    )

    def setup_equipes(self, listeEquipe):
        """
            recupere la liste des equipes

            :param self: le formulaire Jouer
            :param listeEquipe: la liste des equipes
            :type self: Object
            :type listeEquipe: List
        """
        self.equipe.choices.append((None, "Sans équipe"))
        for equipe in listeEquipe:
            self.equipe.choices.append((equipe.idE, equipe.nomE))
