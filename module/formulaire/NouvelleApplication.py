#### HEAD ##############
# Class   : NouvelleApplication
# Content : Formulaire de création d'une nouvelle apparence de l'application


#### IMPORT ############
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Length
from wtforms import StringField, validators
from flask_uploads import IMAGES



#### CLASS #############
class NouvelleApplication(FlaskForm):
    nom = StringField(
        "nom",
        validators = [
            DataRequired("S'il vous plaît, entrer le nom de l'application."),
            Length(min=3, max=50, message="Le nom doit comporter entre 3 et 50 caractères.")
        ]
    )
    logo = FileField(
        "logo",
        validators = [
            DataRequired("S'il vous plaît, entrer le logo de l'application."),
            FileAllowed(IMAGES, 'Seulement des images !')
        ]
    )
    fond = FileField(
        "fond",
        validators = [
            DataRequired("S'il vous plaît, entrer le fond de l'application."),
            FileAllowed(IMAGES, 'Seulement des images !')
        ]
    )


    def __repr__(self):
        return "<NouvelleApplication (%s)>" % (self.nom.data)
