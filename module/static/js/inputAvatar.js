document.addEventListener("DOMContentLoaded", function(){
    elems = document.getElementsByClassName("input-avatar");
    Array.from(elems).forEach(elem => {
        let inputFile = document.getElementById(elem.getAttribute("for"));
        
        inputFile.addEventListener("change", function(event){
            let reader = new FileReader();
            reader.onload = function(e){
                elem.setAttribute("style", "background-image: url('" + e.target.result + "')");
                elem.style.backgroundSize = "cover";
            }
            reader.readAsDataURL(inputFile.files[0])
        });
    });
});