/**
 *  convertit en objet Date les 2 paramètre
 *  @param {string} dateStr : une date au format string "dd / mm / yyyy"
 *  @param {string} heureStr : une heure au format string "hh:mm"
 */
function strToDateHour(dateStr, heureStr){
    let date = new Date();
    date.setFullYear(Number(dateStr.substring(10)));
    date.setMonth(Number(dateStr.substring(5,7))-1);
    date.setDate(Number(dateStr.substring(0, 2)));

    date.setHours(Number(heureStr.substring(0,2)));
    date.setMinutes(Number(heureStr.substring(3)));
    return date;
}

/**
 *  convertit en objet Date les 2 paramètre
 *  @param {string} dateStr : une date au format string "dd / mm / yyyy"
 */
function strToDate (dateStr){
    let date = new Date();
    date.setFullYear(Number(dateStr.substring(10)));
    date.setMonth(Number(dateStr.substring(5,7))-1);
    date.setDate(Number(dateStr.substring(0, 2)));
    return date;
}

// Faits la vérification des dates avec l'horaire inscrit
function testDateComplet(){
    dateDeb = strToDateHour($('input#dateDebut').val(), $('input#heureDebut').val());
    dateFin = strToDateHour($('input#dateFin').val(), $('input#heureFin').val());
    return (dateDeb < dateFin);
}

// Faits la vérification des dates sans l'horaire inscrit
function testDateSeule(){
    dateDeb = strToDate($('input#dateDebut').val());
    dateFin = strToDate($('input#dateFin').val());
    return (dateDeb <= dateFin);
}

// affiche un toast indiquant un problème de date
function toastAlerte(){ //
    let msg = "<div style='text-align: center'> <strong>Date invalide </strong><br/>La date de fin est antérieure celle du début.</div>";
    M.toast({html: msg, classes: "message-warning", displayLength: 5000});
}


/**
 * Appelle la vérification des dates avant la validation des formulaires de concours
 * @return {boolean} : affiche une alerte et refuse la validation du formulaire si les dates sont invalides
*/
$('#formulaireConcours').submit(function() {
    if(testDateComplet())
        return true;

    M.Modal.getInstance(modalErreurDate).open();
    return false;
});

// Appelle le toast d'alerte si les dates entrées sont invalides
function testInputDate(){
    if ($('input#dateDeb').val()!="" && $('input#dateFin').val()!=""){
        if($('input#heureDeb').val()!="" && $('input#heureFin').val()!=""){
            if(!testDateComplet())
                toastAlerte();
        }
        else{
            if(!testDateSeule())
                toastAlerte();
        }
    }
}

// Connexion de testInputDate avec les 4 input de date
$("input#dateDebut").on( "change", testInputDate );
$("input#dateFin").on( "change", testInputDate );
$("input#heureDeb").on( "change", testInputDate );
$("input#heureFin").on( "change", testInputDate );
