document.addEventListener('DOMContentLoaded', function() {
    let listeCouleur = Array(
        "red",
        "pink",
        "indigo",
        "blue",
        "green",
        "yellow",
        "orange",
        "teal",
        "amber",
        "deep-purple",
        "light-blue",
        "light-green",
        "deep-orange"
    )
    let listeAvatar = Array.from(document.getElementsByClassName('team-avatar-no-image'));
    listeAvatar.forEach(elem => {
        if(elem.style.backgroundImage.length == 0){
            let index = Math.trunc(Math.random() * listeCouleur.length);
            let span = elem.children[0];
            span.style.left = (elem.clientWidth/2 - span.clientWidth/2) + "px";
            span.style.top = (elem.clientHeight/2 - span.clientHeight/2) + "px";
            elem.classList.add(listeCouleur[index]);
        }
    });
});